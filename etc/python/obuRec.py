from socket import *

def udpCap():
    #Setup UDP listener
    UDP_IP = "192.168.1.21"
    UDP_PORT = 15030

    udpSock = socket(AF_INET, SOCK_DGRAM) # UDP
    udpSock.bind((UDP_IP, UDP_PORT))

    while  1:
        data, addr = udpSock.recvfrom(1024) # buffer size is 1024 bytes
        print(data)

udpCap()
