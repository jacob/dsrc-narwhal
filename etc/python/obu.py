#Import into the namespace
from socket import *
from subprocess import Popen, PIPE, STDOUT
#Import modules
import base64
import os
import sys
import time
import threading
import hashlib
import requests
import optparse
import signal
import queue
import datetime

def startJAM(freq, gain):
    print("Starting jammer at ", freq, gain)
    file.write(str(gain) + "," + str(freq) + "\n")# + str(data) + "\n")
    jam = Popen(['python3', 'rfReplay210.py', '-f', freq, "-g", gain], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    time.sleep(10)
    print("Jammer Started!")
    return jam
#END startENB

def stopJAM(jammer):
    print("Stopping Jammer", jammer)
    jammer.communicate("\n".encode())
    time.sleep(10)
    print("Jammer Stopped")
#END startENB

def sendData(number, delay):
    for x in range(number):
        os.system("cat " + "mapMsgs/map-" + str((x%20)+1) + ".txt" + "| nc -q 1 -w 1 -u 192.168.1.12 1516")
        time.sleep(delay)

def sendRequest(method, data, conn):
    """Sends a request to on the given socket"""

    req = method + '%' + data + '%ITS\r\n\r\n'

    if verbose: print ("\nSent:\n", req)

    conn.sendall(req.encode())
#END sendRequest

def receive(sock):
    """Ensures that the whole response is read from the socket"""

    #Start with an empty response
    response = ""
    print ("Waiting for response")

    #Loop until the everything is received
    while 1:
        data = sock.recv(1024)
        print ("Received response")

        #Add each new chunk to the response
        response = response + data.decode()

        #Catch the end of the header
        if(r'\r\n\r\n' in response or not data or "\r\n\r\n" in response):
            break

    if verbose: print ("\nReceived: " + response)

    return response
#END receiveAll

def identify(sock):
    """Tells the server what has connected to it"""

    print ("\n\nSending Identity")
    sendRequest('SID', 'OBU', sock)
#END identify

def handleServer(connSock):
    """Handles a jammer over its lifetime"""
    print("Waiting For Inst")

    while True:

        #Get the request from the socket
        data = receive(connSock)

        #Break up the request and parse the URL
        decoded = data
        request = decoded.split('%')

        if verbose: print ("\nReceived: ", request[0], " ", request[1])

        #If the client wants my public key
        if("JAM" in request[0]):
            settings = request[1].split('$')
            Gain = settings[1]
            Freq = settings[0]
            jammer = startJAM(Freq, Gain)
            sendRequest('STR','STARTED',connSock)

        #If the the client returned a secure message
        elif("STP" in request[0]):
            stopJAM(jammer)
            sendRequest('STR','STOPPED',connSock)

        elif("RED" in request[0]):
            print(packets.qsize())
            count = 0
            while packets.qsize() != 0:
                count += 1
                packets.get()
            sendRequest('DTA', str(count) ,connSock)

        elif("SND" in request[0]):
            sendData(100, 1)
            sendRequest('SNS','STOPPED',connSock)

        elif("CSE" in request[0]):
            print("Run Complete")
            exit()
#END handleClient

def udpCap():
    #Setup UDP listener
    UDP_IP = "192.168.1.21"
    UDP_PORT = 15030


    udpSock = socket(AF_INET, SOCK_DGRAM) # UDP
    udpSock.bind((UDP_IP, UDP_PORT))

    while  1:
        data, addr = udpSock.recvfrom(1024) # buffer size is 1024 bytes
        packets.put(data)
        #print("Packet num ", packets.qsize())
        file.write(str(datetime.datetime.now().timestamp()) + "," + str(Gain) + "," + str(Freq) + "\n")# + str(data) + "\n")


#Set up cmd options
cmdParse = optparse.OptionParser()
cmdParse.add_option("-o", "--outfile", action="store", type="str", dest="outfile", default="out.csv",
                    help="Sets the destination file for output: Defaults to out.csv")
cmdParse.add_option("-v", "--verbose", action="store", type="int", dest="verbose", default=1,
                    help="Prints messages on the wire if set to 1: Defaults to 0")
cmdParse.add_option("-a", "--address", action="store", type="str", dest="address", default='pc761.emulab.net',
                    help="Sets the address for the server to bind to")


#Parse the cms and set values
(options, args) = cmdParse.parse_args()
outfile = options.outfile
verbose = options.verbose

host = options.address
port = 2021
Gain = 0
Freq = 0
file = open(outfile, "a")


#Setup output outfile
packets = queue.Queue()

try:#Connect to the server
    print ("\nConnecting to:", host, port)
    clientSock = socket(AF_INET, SOCK_STREAM)
    clientSock.connect((host, port))
    print ("Connected")
except Exception:
    print ("Problem with request closed connection")
    exit()

#Begin for the iperf server
udpcapture = threading.Thread(target=udpCap, args=( ), daemon = False)
udpcapture.start()


identify(clientSock)

handleServer(clientSock)


clientSock.close()
print ("Closed Connection\n\n")
