#Import into the namespace
from socket import *
import numpy as np
from subprocess import Popen, PIPE, STDOUT
#Import modules
import base64
import os
import sys
import time
import threading
import hashlib
import requests
import optparse
import signal
import datetime

def sendRequest(method, data, conn):
    """Sends a request to on the given socket"""

    req = method + '%' + data + '%ITS\r\n\r\n'

    conn.sendall(req.encode())
#END sendRequest

def receive(sock):
    """Ensures that the whole response is read from the socket"""

    #Start with an empty response
    response = ""
    moredata = True

    #Loop until the everything is received
    while moredata:
        data = sock.recv(1024)
        #print "Received response"

        #Add each new chunk to the response
        response = response + data.decode()
        #print response
        #Catch the end of the header
        if(r'\r\n\r\n' in response or "\r\n\r\n" in response or '%ITS' in response):
            moredata = False

        if len(data) == 0:
            return "CSE"

    if verbose: print ("\nReceived: " + response)

    return response
#END receiveAll

def collectControl(sockRSU, sockOBU):
    """Sends a request for packets and counts number of received packets"""
    sendRequest('SND', "100$0.1", sockRSU)#Request set of packets
    data = receive(sockRSU)
    print("Finished sending... Counting results")
    sendRequest("RED", "STOP", sockOBU)
    count = receive(sockOBU)
    print("Packet Count", count.split('%')[1])
    file.write("0" + "e3," + "0" + "," + count.split('%')[1]+ "\n")

def handleJammer(sockRSU, sockOBU):
    """Handles the running of the experiment"""
    print("Starting experiment\n")

    for i in range(2):
        #Collect control samples before the jammer is ever turned on
        for j in range(3):
            collectControl(sockRSU, sockOBU)

        #Run the experiment
        for f in range(5800000, 5900000, 1000):#Start at cmd specified freq and go to cmd specified freq
            for g in range(51, 61, 1):#Start at cmd specified gain and go to max
                #input("Waiting for input")
                print("Starting jammer at " + str(f) + " " + str(g))#Give the user a readout of current settings
                sendRequest('JAM', (str(f)+'e3$'+str(g)), sockOBU)#Request the OBU to start the jammer
                time.sleep(10)#Give jammer time to start
                receive(sockOBU)#Pull started response
                #input("Waiting for jammer test")
                sendRequest('SND', "1000$0.1", sockRSU)#Request set of packets from RSU

                while True:

                    #Get the request from the socket
                    data = receive(sockRSU)

                    #Break up the response
                    decoded = data
                    request = decoded.split('%')

                    #print "\nReceived: ", decoded

                    if("SID" in request[0]):#Tells the server who the connection is coming from
                        print("Identifier Received")

                    elif("SNS" in request[0]):#Processes the result coming from the OBU
                        print("Finished sending... Counting results")
                        sendRequest("RED", "STOP", sockOBU)
                        count = receive(sockOBU)
                        print("Packet Count", count.split('%')[1])
                        file.write(str(f) + "e3," + str(g) + "," + count.split('%')[1]+ "\n")
                        break

                    elif("STR" in request[0]):
                        if("STARTED" in request[1]):
                            print("Jammer Started")
                            sendRequest("STP", "STOP", connSock)
                        if("STOPPED" in request[1]):
                            print("Jammer Stopped")
                            break

                time.sleep(5)
                sendRequest('STP', 'Stop', sockOBU)
                receive(sockOBU)

    print("End loop")
    sendRequest("CSE", "DONE", sockOBU)
    sendRequest("CSE", "DONE", sockRSU)
    file.close()

    sys.exit()
#END handleClient

def handleClient(connSock):
    """Handles a connection over its lifetime"""

    while True:

        #Get the request from the socket
        data = receive(connSock)

        #Break up the request and parse the URL
        decoded = data
        request = decoded.split('%')

        print ("\nReceived: ", request[0], request[1])

        #If the client wants my public key
        if("SID" in request[0]):
            print("Identifier Received")
            if("RSU" in request[1]):
                print("RSU")
                #handleJammer(connSock)
                return "RSU"
            elif("OBU" in request[1]):
                print("OBU")
                #handleUE(connSock)
                return "OBU"


#END handleClient

def receiveUDP():
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        print("received message: %s" % data.decode())
        file.write(data+"\n")

    print ("The server can receive")


#Set up cmd options
cmdParse = optparse.OptionParser()
cmdParse.add_option("-o", "--outfile", action="store", type="str", dest="outfile", default="out",
                    help="Sets the destination file for output with .csv type: Defaults to out")
cmdParse.add_option("-v", "--verbose", action="store", type="int", dest="verbose", default=0,
                    help="Prints messages on the wire if set to 1: Defaults to 0")
cmdParse.add_option("-a", "--address", action="store", type="str", dest="address", default="155.98.36.61",
                    help="Sets the address for the server to bind to")
cmdParse.add_option("-f", "--frequency", action="store", type="int", dest="freqStart", default=5600000,
                    help="Sets the the starting frequency for sweep")
cmdParse.add_option("-s", "--stopFreq", action="store", type="int", dest="freqStop", default=6000000,
                    help="Sets the the stoping frequency for sweep")
cmdParse.add_option("-g", "--gain", action="store", type="int", dest="gainStart", default=0,
                    help="Sets the the starting gain for sweep, will add 10 every iteration")

#Parse the cms and set values
(options, args) = cmdParse.parse_args()
outfile = options.outfile
verbose = options.verbose
servAdd = options.address
freqStr = options.freqStart
freqStp = options.freqStop
gainStr = options.gainStart

rF = False
oF = False

#Setup output outfile
file = open("results/dataSets/" + outfile + ".csv", "a")#Open file for results
file.write("Frequency,Gain,PDR\n")#Add file header for pandas


#Set up listener
servSock = socket(AF_INET, SOCK_STREAM)
servSock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)#Reuse the socket
servSock.bind((servAdd, 2021))#Bind the socket
servSock.listen(1)#Start listening for incoming connections

#Setup UDP listener
UDP_IP = "localhost"
UDP_PORT = 15030

udpSock = socket(AF_INET, SOCK_DGRAM) # UDP
udpSock.bind((UDP_IP, UDP_PORT))

while (not rF or not oF):
    print ("Waiting")
    connSock, addr = servSock.accept()
    print ("Accepted new client\n")

    #Begin a new thread for each client
    #x = threading.Thread(target=handleClient, args=(connSock, ), daemon=True)
    #x.start()
    nammed = handleClient(connSock)

    if(nammed == "RSU"):
        jmSock = connSock
        rF = True

    elif(nammed == "OBU"):
        ueSock = connSock
        oF = True

print ("Have both RSU and OBU")

handleJammer(jmSock, ueSock)
#END main
