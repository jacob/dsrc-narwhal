import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

column_subset = ["Frequency", "Gain", "PDR"]

def hz2GHz(freq):
    return (freq *1e-9)

halfDSRC = (8.3/2) * 1e-3
halfWiFi = (16.6/2) * 1e-3

#f, g, pdr = np.loadtxt('test5.csv', delimiter=',', unpack=True)
#plt.plot(hz2GHz(f),pdr, label='WiFi Interference (20MHz Channel, TX gain 88)', c='r')

df = pd.read_csv("freq4Test.csv", usecols=column_subset)
df.sort_values(by=["Frequency", "Gain"], inplace=True)

plt.plot(df.Gain[0:89],df.PDR[0:89]/10, label='WiFi Interference (20MHz Channel, TX freq 5.60GHz(Uninterfered)', c='c')
plt.plot(df.Gain[0+(90):89+(90)],df.PDR[0+(90):89+90]/10, label='WiFi Interference (20MHz Channel, TX freq 5.80GHz(Aliased)', c='m')
plt.plot(df.Gain[0+(90*2):89+(90*2)],df.PDR[0+(90*2):89+(90*2)]/10, label='WiFi Interference (20MHz Channel, TX freq 5.86GHz(Interfered)', c='y')
plt.plot(df.Gain[0+(90*3):89+(90*3)],df.PDR[0+(90*3):89+(90*3)]/10, label='WiFi Interference (20MHz Channel, TX freq 5.88GHz(Shoulder)', c='k')

#f, g, pdr = np.loadtxt('test6.csv', delimiter=',', unpack=True)
#plt.plot(hz2GHz(f),pdr, label='WiFi Interference (20MHz Channel, TX gain 68)', c='b')
#plt.axvline(x=(5.86 - halfDSRC - halfWiFi), label='Direct {} Interference Boundary'.format("WiFi"), c='k', linestyle='--')
#plt.axvline(x=(5.86 + halfDSRC + halfWiFi),  c='k',  linestyle='--')

plt.xlabel('Interference TX Gain (dB)')
plt.ylabel('Packet Delivery Ratio (PDR)')
plt.title('Interference Impact\nRSU(Browning) -> OBU(WEB)\nControl PDR 99.44%')
plt.legend()
plt.tight_layout()
plt.show()
