import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

column_subset = ["Frequency", "Gain", "PDR"]

def hz2GHz(freq):
    return (freq *1e-9)

halfDSRC = (8.3/2) * 1e-3
halfWiFi = (16.6/2) * 1e-3

#f, g, pdr = np.loadtxt('test5.csv', delimiter=',', unpack=True)
#plt.plot(hz2GHz(f),pdr, label='WiFi Interference (20MHz Channel, TX gain 88)', c='r')

df = pd.read_csv("panda1.csv", usecols=column_subset, nrows=500)
df.sort_values(by=["Gain", "Frequency"])[["Gain", "Frequency"]]

plt.plot(hz2GHz(df.Frequency[100:500]),df.PDR[100:500], label='WiFi Interference (20MHz Channel, TX gain 68)', c='b')

#f, g, pdr = np.loadtxt('test6.csv', delimiter=',', unpack=True)
#plt.plot(hz2GHz(f),pdr, label='WiFi Interference (20MHz Channel, TX gain 68)', c='b')


plt.axvline(x=(5.86 - halfDSRC - halfWiFi), label='Direct {} Interference Boundary'.format("WiFi"), c='k', linestyle='--')
plt.axvline(x=(5.86 + halfDSRC + halfWiFi),  c='k',  linestyle='--')

plt.xlabel('Interference Center Frequency (GHz)')
plt.ylabel('Packet Delivery Ratio (PDR)')
plt.title('Interference Impact\nRSU(Browning) -> OBU(WEB)\nControl PDR 99.44%')
plt.legend()
plt.tight_layout()
plt.show()
