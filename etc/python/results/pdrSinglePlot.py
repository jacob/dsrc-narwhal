import matplotlib.pyplot as plt
import numpy as np

def hz2GHz(freq):
    return (freq *1e-9)

f, g, pdr = np.loadtxt('smFrq.csv', delimiter=',', unpack=True)
plt.plot(g,pdr, label='WiFi Interference (20MHz Channel at 5.68GHz = Direct)', c='r')

f, g, pdr = np.loadtxt('ail2.csv', delimiter=',', unpack=True)
plt.plot(g,pdr, label='WiFi Interference (20MHz Channel at 5.75GHz = Alised)', c='b')

plt.xlabel('Interference TX Gain')
plt.ylabel('Packet Delivery Ratio (PDR)')
plt.title('Interference Impact\nRSU(Browning) -> OBU(WEB)\nControl PDR 99.44%')
plt.legend()
plt.tight_layout()
plt.show()
