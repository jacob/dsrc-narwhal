import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

column_subset = ["Frequency", "Gain", "PDR"]

def hz2GHz(freq):
    return (freq *1e-9)

halfDSRC = (8.3/2) * 1e-3
halfWiFi = (16.6/2) * 1e-3

files = ["realAvg"]

for file in files:
    df = pd.read_csv("dataSets/"+file+".csv", usecols=column_subset)
    df.sort_values(by=["Frequency", "Gain"], inplace=True)

    gain = df.Gain.to_numpy()
    pdr = df.PDR.to_numpy()

    plt.plot(gain,pdr/10, label="From {}".format(file))

plt.xlabel('Interference TX Gain (dB)')
plt.ylabel('Packet Delivery Ratio (PDR)')
plt.title('Interference Impact (20MHz Channel, TX freq 5.845GHz)\nRSU(Browning) -> OBU(WEB)\nControl PDR 99.44%')
plt.legend()
plt.tight_layout()
plt.show()
