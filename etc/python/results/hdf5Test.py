import h5py as h5

file = h5.File("testFile.hdf5", "w")

dset = file.create_dataset("myDataset", (1201, 3), dtype="f")

print(dset.shape, dset.dtype)

file.close()
