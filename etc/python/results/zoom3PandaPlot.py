import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import pandas as pd

column_subset = ["Frequency", "Gain", "PDR"]

def lIndex(i):
    return (99*(i+1))

def rIndex(i):
    return 99+(99*(i+1))

def hz2GHz(freq):
    return (freq *1e-9)

halfDSRC = (8.3/2) * 1e-3
halfWiFi = (16.6/2) * 1e-3

#f, g, pdr = np.loadtxt('test5.csv', delimiter=',', unpack=True)
#plt.plot(hz2GHz(f),pdr, label='WiFi Interference (20MHz Channel, TX gain 88)', c='r')

df = pd.read_csv("dataSets/cleanZoom.csv", usecols=column_subset)
df.sort_values(by=["Gain", "Frequency"], inplace=True)

freq = df.Frequency.to_numpy()
pdr = df.PDR.to_numpy()

# Gain Values
gains = range(51, 61, 1)

for j,g in zip(range(10), gains):
    plt.plot(hz2GHz(freq[lIndex(j):rIndex(j)]),pdr[lIndex(j):rIndex(j)]/10, label='TX gain {}'.format(g))

df = pd.read_csv("dataSets/g88.csv", usecols=column_subset)
freq = df.Frequency.to_numpy()
pdr = df.PDR.to_numpy()
plt.plot(hz2GHz(freq),pdr, label='TX gain {}'.format("88"), linestyle="dotted", color='r')

#Interference Boundary Lines
#plt.axvline(x=(5.86 - halfDSRC - halfWiFi), label='Direct {} Interference Boundary'.format("WiFi"), c='k', linestyle='--')
#plt.axvline(x=(5.86 + halfDSRC + halfWiFi),  c='k',  linestyle='--')

#Real Boundary Lines
#plt.axvline(x=(5.845), label='Realistic {} Center Frequency'.format("WiFi"), c='r', linestyle='--')
#plt.axvline(x=(5.86), label='Realistic {} Center Frequency'.format("DSRC"), c='b', linestyle='--')

#Add rectangle
left, bottom, width, height = ((5.845 - halfWiFi), -5, ((5.845 + halfWiFi)-(5.845 - halfWiFi)), 110)
rect=patches.Rectangle((left,bottom),width,height,
                        #fill=False,
                        alpha=0.1,
                        facecolor="red")

plt.gca().add_patch(rect)
plt.text(5.846, bottom + 2,'WiFi Band',fontsize=6, color="red", weight="bold")
plt.axvline(x=(5.845), c='r', linestyle='--')


#Add rectangle
left, bottom, width, height = ((5.86 - halfDSRC), -5, ((5.86 + halfDSRC)-(5.86 - halfDSRC)), 110)
rect=patches.Rectangle((left,bottom),width,height,
                        #fill=False,
                        alpha=0.1,
                        facecolor="blue")

plt.gca().add_patch(rect)
plt.text(5.861, bottom + 2,'DSRC Band',fontsize=6, color="blue", weight="bold")
#plt.axvline(x=(5.86), c='b', linestyle='--')


plt.xlabel('Interference TX Center Frequency (GHz)')
plt.ylabel('Packet Delivery Ratio (PDR)')
plt.title('Interference Impact\nRSU(Browning) -> OBU(WEB)\nControl PDR 99.44%')
plt.legend()
plt.tight_layout()
plt.show()
