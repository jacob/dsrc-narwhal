import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

column_subset = ["Frequency", "Gain", "PDR"]

def lIndex(i):
    return 199 + (400*i)

def rIndex(i):
    return 299 + (400*i)

def hz2GHz(freq):
    return (freq *1e-9)

halfDSRC = (8.3/2) * 1e-3
halfWiFi = (16.6/2) * 1e-3

#f, g, pdr = np.loadtxt('test5.csv', delimiter=',', unpack=True)
#plt.plot(hz2GHz(f),pdr, label='WiFi Interference (20MHz Channel, TX gain 88)', c='r')

df = pd.read_csv("dataSets/timeTestFull1.csv", usecols=column_subset)
df.sort_values(by=["Gain", "Frequency"], inplace=True)

# colors for the lines
colors = ['r','g','b','c','m','y','k','o']
# Gain Values
gains = [40, 45, 50, 55, 60, 65, 70, 89]

for j,c,g in zip(range(8), colors, gains):
    plt.plot(hz2GHz(df.Frequency[lIndex(j):rIndex(j)]),df.PDR[lIndex(j):rIndex(j)]/10, label='WiFi Interference (20MHz Channel, TX gain {}'.format(g))

#Interference Boundary Lines
plt.axvline(x=(5.86 - halfDSRC - halfWiFi), label='Direct {} Interference Boundary'.format("WiFi"), c='k', linestyle='--')
plt.axvline(x=(5.86 + halfDSRC + halfWiFi),  c='k',  linestyle='--')

plt.xlabel('Interference TX Center Frequency (GHz)')
plt.ylabel('Packet Delivery Ratio (PDR)')
plt.title('Interference Impact\nRSU(Browning) -> OBU(WEB)\nControl PDR 99.44%')
plt.legend()
plt.tight_layout()
plt.show()
