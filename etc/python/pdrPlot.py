import matplotlib.pyplot as plt
import numpy as np

def offset(freq):
    return ((2132500 - freq) * .001)

def hz2GHz(freq):
    return (freq *1e-9)

def band(input):
    return (((25*.18)/2) + input/2)

f, g, pdr = np.loadtxt('test1.csv', delimiter=',', unpack=True)
plt.plot(hz2GHz(f),pdr, label='WiFi Interference (20MHz Channel)', c='r')

# x coordinates for the lines
#xcoords = [band(16.6), band(25*.18), band(50*.18)]
# colors for the lines
#colors = ['r','g','b']
# labels for lines
#label = ["WiFi", "5MHz LTE", "10MHz LTE"]

#for xc,c,l in zip(xcoords,colors, label):
#    plt.axvline(x=xc, label='Direct {} Interference Boundary'.format(l), c=c, linestyle='--')
    #plt.axvline(x=-(xc),  c=c,  linestyle='--')

plt.xlabel('Interference Center Frequency (GHz)')
plt.ylabel('Packet Delivery Ratio (PDR)')
plt.title('Interference Impact\nRSU(Browning) -> OBU(WEB)')
plt.legend()
plt.tight_layout()
plt.show()
