Cohda RSU PCAP Logging Instructions

SSH login: rsu
password: rsuadmin

***********************************************************************************
Check PCAP logging status
***********************************************************************************
snmpwalk -v3 -lauthPriv -ursu -Arsuadmin -Xrsuadmin -aSHA -xAES -mRSU-MIB -M/mnt/rw/rsu1609/snmp/mibs -O T 127.0.0.1 rsuInterfaceLogTable

***********************************************************************************
Enable PCAP Logging
***********************************************************************************
Set operate mode to standby:  
snmpset -v3 -lauthPriv -ursu -Arsuadmin -Xrsuadmin -aSHA -xAES -mRSU-MIB -M/mnt/rw/rsu1609/snmp/mibs -O T 127.0.0.1 rsuMode.0 i 2

Enable PCAP Logging:
snmpset -v3 -lauthPriv -ursu -Arsuadmin -Xrsuadmin -aSHA -xAES -mRSU-MIB -M/mnt/rw/rsu1609/snmp/mibs -O T 127.0.0.1 \
rsuIfaceGenerate.2 i  1 \
rsuIfaceMaxFileSize.2 i 40 \
rsuIfaceGenerate.3 i  1 \
rsuIfaceMaxFileSize.3 i 40 \
rsuIfaceGenerate.4 i  1 \
rsuIfaceMaxFileSize.4 i 40 \
rsuIfaceGenerate.5 i  1 \
rsuIfaceMaxFileSize.5 i 40 \
rsuMode.0 i 4

***********************************************************************************
Verify logging, watching PCAP file sizes grow:
***********************************************************************************
watch ls -alt /mnt/rw/log/current/

***********************************************************************************
Disable PCAP Logging
***********************************************************************************
Set operate mode to standby:  
snmpset -v3 -lauthPriv -ursu -Arsuadmin -Xrsuadmin -aSHA -xAES -mRSU-MIB -M/mnt/rw/rsu1609/snmp/mibs -O T 127.0.0.1 rsuMode.0 i 2

Disable PCAP Logging:
snmpset -v3 -lauthPriv -ursu -Arsuadmin -Xrsuadmin -aSHA -xAES -mRSU-MIB -M/mnt/rw/rsu1609/snmp/mibs -O T 127.0.0.1 \
rsuIfaceGenerate.2 i  0 \
rsuIfaceGenerate.3 i  0 \
rsuIfaceGenerate.4 i  0 \
rsuIfaceGenerate.5 i  0 \
rsuMode.0 i 4

***********************************************************************************
Restart logging after 24 hours
***********************************************************************************
Option 1 - Restart application:
/opt/cohda/application/rc.local restart

Option 2 - Reboot RSU:
sudo reboot

Option 3 - Disable and re-enable logging (see above)

Option 4 - Power cycle the RSU

***********************************************************************************
PCAP Log File Locations
***********************************************************************************
Current PCAP log files: /mnt/rw/log/current/

Old PCAP log files: /mnt/rw/log/