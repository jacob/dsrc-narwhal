import os
import time
from socket import *

def netcat(filename):
    """Sends packet to the rsu for transmission"""
    s = socket(AF_INET, SOCK_DGRAM)#Open UDP sock
    s.connect(("192.168.1.12", 1516))#Connect to RSU
    s.sendall(open(filename, "r").read().encode())#Read in data to send
    s.shutdown(SHUT_WR)#Close socket


for x in range(1, 21, 1):
    netcat("mapMsgs/map-" + str(x) + ".txt")
    time.sleep(0.1)

print("FIN\n")
