import socket

UDP_IP = "192.168.1.11"
UDP_PORT = 15050

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

x = 0

while True:
    x = x+1
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print(str(x))
