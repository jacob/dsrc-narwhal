#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.urn as URN

tourDescription = """
# Narwhal DSRC Setup
Sets up an experiment that connects to the Narwhal DSRC equipment. On the rooftop

The following devices will be available at these addesses:
Connected to node1
* RSU: 192.168.1.12
    Login: rsu/rsuadmin

* RSP: 192.168.1.11
    Login: N/A

* OBU: 192.168.1.22
    Login: admin/P@ssw0rd

* OBP: 192.168.1.21
    Login: helix/its101
"""

tourInstructions = """
### Lear OBU PCAP Logging Instructions
After your experiment becomes ready, login to `OBU` via `ssh`
```
ssh admin@192.168.1.22
# password: P@ssw0rd
```

Enable pcap logging
```
# Enter configuration mode
enable
# password: Priv@321#

# Enable PCAP logging on radio 1 (wifi0vap0)
config locos logging interface1 status enable

# Enable PCAP logging on radio 2 (wifi1vap0)
config locos logging interface2 status enable
```

Check logging is enabled from info mode
```
show locos logging
```

Access PCAP files that have been logged
```
# Enter request mode
Request
# password: Priv@321#

# Enter shell
request system shell

# CD to logging directory
cd /var/storage/wlan_capture/
```

Disable PCAP logging
```
# Enter configuration mode
enable

# Enable mode password
Priv@321#

# Disable PCAP logging on radio 1 (wifi0vap0)
config locos logging interface1 status disable

# Disable PCAP logging on radio 2 (wifi1vap0)
config locos logging interface2 status disble
```

(Not Currently Supported) Secure copy PCAP files to server at 192.168.1.122 directory /home/helix/temp
```
scp -r *.pcap bill3@192.168.1.122:~/temp
```

### Cohda RSU PCAP Logging Instructions
After your experiment becomes ready, login to `RSU` via `ssh`
```
ssh rsu@192.168.1.12
# password: rsuadmin
```

Check PCAP logging status
```
snmpwalk -v3 -lauthPriv -ursu -Arsuadmin -Xrsuadmin -aSHA -xAES -mRSU-MIB -M/mnt/rw/rsu1609/snmp/mibs -O T 127.0.0.1 rsuInterfaceLogTable
```

Verify logging, watching PCAP file sizes grow
```
watch ls -alt /mnt/rw/log/current/
```

Restart logging after 24 hours
```
/opt/cohda/application/rc.local restart
```
or
```
sudo reboot
```

PCAP Log File Locations
```
# Current PCAP log files
cd /mnt/rw/log/current/

# Old PCAP log files
cd /mnt/rw/log/
```

"""

request = portal.context.makeRequestRSpec()

#Define globals to be used
class GLOBALS(object):
    IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU20-64-STD"
    HWTYPE = "d430"

#Create the LAN
nodelink = request.Link("nodelink")

# Create two raw "PC" nodes
node1 = request.RawPC("node1") #Request a physical node
node1.disk_image = GLOBALS.IMG #Specify node image
node1.hardware_type = GLOBALS.HWTYPE #Specify hardware type
node1Lan = node1.addInterface("nodeLan") #Add interface to node
node1Lan.addAddress(rspec.IPv4Address("10.10.2.1", "255.255.255.0")) #Assign IP to the interface
nodelink.addInterface(node1Lan) #Add to the lan

node2 = request.RawPC("node2") #Request a physical node
node2.disk_image = GLOBALS.IMG #Specify node image
node2.hardware_type = GLOBALS.HWTYPE #Specify hardware type
node2Lan = node2.addInterface("nodeLan") #Add interface to node
node2Lan.addAddress(rspec.IPv4Address("10.10.2.2", "255.255.255.0")) #Assign IP to the interface
nodelink.addInterface(node2Lan) #Add to the lan

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

portal.context.printRequestRSpec()
